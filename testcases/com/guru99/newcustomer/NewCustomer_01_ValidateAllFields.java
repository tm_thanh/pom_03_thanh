package com.guru99.newcustomer;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.xml.DOMConfigurator;
import org.automationtesting.excelreport.Xl;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.Constants;
import commons.LogEvent;
import msg.Msg_CustomerField;
import pages.HomePage;
import pages.LoginPage;
import pages.NewCustomerPage;
import pages.PageFactory;
import testdata.Customer;

public class NewCustomer_01_ValidateAllFields extends AbstractTest {
	WebDriver driver;
	HomePage homePage;
	LoginPage loginPage;
	NewCustomerPage newcustomerPage;
	LogEvent LOG;

	@Parameters({ "browser", "url" })
	@BeforeClass
	public void beforeClass(String browser, String url) {
		driver = openBrowser(browser, url);
		loginPage = PageFactory.openLoginPage(driver);
		DOMConfigurator.configure("resources\\log4j.xml");
		homePage = loginPage.LoginAs(Constants.USERNAME, Constants.PASSWORD);
		newcustomerPage = homePage.openNewCustomerPage(driver);
		// homePage = newcustomerPage.openHomePage(driver);
		LOG = new LogEvent();
	}

	@Test
	public void TC_01_Name_cannot_be_empty() {

		LOG.info("1) Do not enter a value in NAME Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueIntoNAMEField();

		LOG.info("==> An error message \"Customer name must not be blank\" must shown");
		assertEquals(newcustomerPage.getMessageOfNameField(), Msg_CustomerField.NAME_EMPTY);

	}

	@Test
	public void TC_02_Name_cannot_be_numeric() {

		LOG.info("1) Enter numeric value in NAME Field");
		newcustomerPage.enterValueToName(Customer.numberic);

		LOG.info("==> An error message \"Numbers are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfNameField(), Msg_CustomerField.NUMBERIC);

	}

	@DataProvider(name = "testdata")
	public static Object[][] TestDataFeed() {
		return new Object[][] { { "name!@#" }, { "@#@#" }, { "3343#$" } };
	}

	@Test(dataProvider = "testdata")
	public void TC_03_Name_cannot_have_special_characters(String a) {

		LOG.info("1) Enter Special Character In NAME Field");
		newcustomerPage.enterValueToName(a);

		LOG.info("==> An error message \"Special characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfNameField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_04_Name_cannot_have_first_character_as_blank_space() {

		LOG.info("1) Enter First character Blank space");
		newcustomerPage.enterValueToName(" ");

		LOG.info("==> An error message \"First character can not have space\" must be shown");
		assertEquals(newcustomerPage.getMessageOfNameField(), Msg_CustomerField.SPACE_FIRST);

	}

	@Test
	public void TC_05_Address_cannot_be_empty() {

		LOG.info("1) Do not enter a value in ADDRESS Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueToAddress();

		LOG.info("==> An error message \"Address Field must not be blank\" must be shown");
		assertEquals(newcustomerPage.getMessageOfAddressField(), Msg_CustomerField.ADDR_EMPTY);

	}

	@Test
	public void TC_06_Address_cannot_have_first_blank_space() {

		LOG.info("1) Enter First character Blank space");
		newcustomerPage.enterValueToAddress(" ");

		LOG.info("==> An error message \"First character cannot be space\" must be shown");
		assertEquals(newcustomerPage.getMessageOfAddressField(), Msg_CustomerField.SPACE_FIRST);

	}

	@Test
	public void TC_07_City_cannot_be_empty() {

		LOG.info("1) Do not enter a value in CITY Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueToCity();

		LOG.info("==> An error message \"City Field must be not blank\" must be shown");
		assertEquals(newcustomerPage.getMessageOfCityField(), Msg_CustomerField.CITY_EMPTY);

	}

	@Test
	public void TC_08_City_cannot_be_numeric() {

		LOG.info("1) Enter numeric value in CITY Field");
		newcustomerPage.enterValueToCity(Customer.numberic);

		LOG.info("==> An error message \"Numbers are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfCityField(), Msg_CustomerField.NUMBERIC);

	}

	@Test
	public void TC_09_City_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In CITY Field");
		newcustomerPage.enterValueToCity(Customer.special);

		LOG.info("==> An error message \"Special characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfCityField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_10_City_cannot_have_first_blank_space() {

		LOG.info("1) Enter First character Blank space");
		newcustomerPage.enterValueToCity(" ");

		LOG.info("==> An error message \"First character cannot be space\" must be shown");
		assertEquals(newcustomerPage.getMessageOfCityField(), Msg_CustomerField.SPACE_FIRST);

	}

	@Test
	public void TC_11_State_cannot_be_empty() {

		LOG.info("1) Do not enter a value in STATE Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueToState();

		LOG.info("==> An error message \"State must not be blank\" must be shown");
		assertEquals(newcustomerPage.getMessageOfStateField(), Msg_CustomerField.STATE_EMPTY);

	}

	@Test
	public void TC_12_State_cannot_be_numeric() {

		LOG.info("1) Enter numeric value in STATE Field");
		newcustomerPage.enterValueToState(Customer.numberic);

		LOG.info("==> An error message \"Numbers are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfStateField(), Msg_CustomerField.NUMBERIC);

	}

	@Test
	public void TC_13_State_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In STATE Field");
		newcustomerPage.enterValueToState(Customer.special);

		LOG.info("==> An error message \"Special characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfStateField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_14_State_cannot_have_first_blank_space() {

		LOG.info("1) Enter First character Blank space");
		newcustomerPage.enterValueToState(" ");

		LOG.info("==> An error message \"First character cannot be space\" must be shown");
		assertEquals(newcustomerPage.getMessageOfStateField(), Msg_CustomerField.SPACE_FIRST);

	}

	@Test
	public void TC_15_PIN_cannot_be_empty() {

		LOG.info("1) Do not enter a value in PIN Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueToPIN();

		LOG.info("==> An error message \"PIN code must not be blank\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPINField(), Msg_CustomerField.PIN_EMPTY);

	}

	@Test
	public void TC_16_PIN_must_be_numeric() {

		LOG.info("1) Enter character value in PIN Field");
		newcustomerPage.enterValueToPIN(Customer.Character);

		LOG.info("==> An error message \"Characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPINField(), Msg_CustomerField.CHARACTER);

	}

	@Test
	public void TC_17_PIN_must_have_6_digits() {

		LOG.info("1) Enter More than 6 digit in PIN field");
		newcustomerPage.enterValueToPIN(Customer.Pin_short);

		LOG.info("==> An error message \"PIN Code must have 6 Digits\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPINField(), Msg_CustomerField.PIN_SHORT);

		LOG.info("2) Enter Less Than 6 digit in PIN field");
		newcustomerPage.enterValueToPIN(Customer.Pin);

	}

	@Test
	public void TC_18_PIN_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In PIN Field");
		newcustomerPage.enterValueToPIN(Customer.special);

		LOG.info("==> An error message \"Special characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPINField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_19_PIN_cannot_have_first_blank_space() {

		LOG.info("1) Enter First character Blank space");
		newcustomerPage.enterValueToPIN(" ");

		LOG.info("==> An error message \"First character cannot be space\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPINField(), Msg_CustomerField.SPACE_FIRST);

	}

	@Test
	public void TC_20_PIN_cannot_have_blank_space() {
		// testcase is unclear
	}

	@Test
	public void TC_21_Telephone_cannot_be_empty() {

		LOG.info("1) Do not enter a value in Telephone Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueToPhone();

		LOG.info("==> An error message \"Telephone no must not be blank\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPhoneField(), Msg_CustomerField.PHONE_EMPTY);

	}

	@Test
	public void TC_22_Telephone_cannot_have_first_character_as_blank_space() {

		LOG.info("1) Enter First character Blank space");
		newcustomerPage.enterValueToPhone(" ");

		LOG.info("==> An error message \"First character cannot be space\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPhoneField(), Msg_CustomerField.SPACE_FIRST);

	}

	@Test
	public void TC_23_Telephone_cannot_have_spaces() {

		LOG.info("1) Enter Blank space in Telephone");
		newcustomerPage.enterValueToPhone(Customer.number_space);

		LOG.info("==> An error message \"Characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPhoneField(), Msg_CustomerField.CHARACTER);

	}

	@Test
	public void TC_24_Telephone_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In Telephone Field");
		newcustomerPage.enterValueToPhone(Customer.special);

		LOG.info("==> An error message \"Special characters are not allowed\" must be shown");
		assertEquals(newcustomerPage.getMessageOfPhoneField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_25_Email_cannot_be_empty() {

		LOG.info("1) Do not enter a value in Email Field");
		LOG.info("2) Press TAB and move to next Field");
		newcustomerPage.notEnterValueToEmail();

		LOG.info("==> An error message \"Email ID must not be blank\" must be shown");
		assertEquals(newcustomerPage.getMessageOfEmailField(), Msg_CustomerField.EMAIL_EMPTY);

	}

	@Test
	public void TC_26_Email_must_be_in_correct_format() {

		LOG.info("Enter invalid email in Email field");
		newcustomerPage.enterValueToEmail(Customer.invalid_email);

		LOG.info("==> An error message \"Email-ID is not valid\" must be shown");
		assertEquals(newcustomerPage.getMessageOfEmailField(), Msg_CustomerField.EMAIL_INVALID);

	}

	@Test
	public void TC_27_Email_cannot_have_space() {

		LOG.info("1) Enter Blank space in Email");
		newcustomerPage.enterValueToEmail(Customer.email_space);

		LOG.info("==> An error message \"Email-ID is not valid\" must be shown");
		assertEquals(newcustomerPage.getMessageOfEmailField(), Msg_CustomerField.EMAIL_SPACE);

	}

	@AfterClass
	public void afterClass() throws Exception {
		Xl.generateReport(Constants.EXCEL_PATH, Constants.EXCEL_NAME);
		driver.quit();
	}

}
