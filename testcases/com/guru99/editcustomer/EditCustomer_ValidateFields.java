package com.guru99.editcustomer;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.xml.DOMConfigurator;
import org.automationtesting.excelreport.Xl;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.AbstractTest_cloud;
import commons.Constants;
import commons.LogEvent;
import msg.Msg_CustomerField;
import pages.EditCustomerPage;
import pages.HomePage;
import pages.LoginPage;
import pages.PageFactory;
import testdata.Customer;

public class EditCustomer_ValidateFields extends AbstractTest_cloud {
	WebDriver driver;
	HomePage homePage;
	LoginPage loginPage;
	EditCustomerPage editcustomerPage;
	LogEvent LOG;

	@Parameters({ "browser", "url" })
	@BeforeClass
	public void beforeClass(String browser, String url) throws Exception {
		driver = openBrowser(browser, url);
		loginPage = PageFactory.openLoginPage(driver);
		DOMConfigurator.configure("resources\\log4j.xml");
		homePage = loginPage.LoginAs(Constants.USERNAME, Constants.PASSWORD);
		editcustomerPage = homePage.openEditCustomerPage(driver);
		LOG = new LogEvent();
	}

	// test data
	String editcustomerURL = "http://demo.guru99.com/v4/manager/editCustomerPage.php";

	@Test
	public void TC_01_Customer_id_cannot_be_empty() {

		LOG.info("1) Do not enter a value in Customer id Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.notEnterValueToCustomerID();

		LOG.info("==> An error message \"Customer ID is required\" must be shown");
		assertEquals(editcustomerPage.getMessageOfCustomerIDField(), Msg_CustomerField.CUSTOMER_EMPTY);

	}

	@Test
	public void TC_02_Customer_id_must_be_numeric() {

		LOG.info("1) Enter character value in Customer id Field");
		editcustomerPage.enterValueToCustomerID(Customer.Character);

		LOG.info("==> An error message \"Characters are_not_allowed\" must be shown");
		assertEquals(editcustomerPage.getMessageOfCustomerIDField(), Msg_CustomerField.CHARACTER);

	}

	@Test
	public void TC_03_Customer_id_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In Customer id Field");
		editcustomerPage.enterValueToCustomerID(Customer.special);

		LOG.info("==> An error message \"Special characters are not allowed\" must be shown");
		assertEquals(editcustomerPage.getMessageOfCustomerIDField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_04_Valid_Customer_Id() {

		LOG.info("1) Enter valid Customer id");
		editcustomerPage.enterValueToCustomerID(Constants.CUSTOMERID);

		LOG.info("2) Submit");
		editcustomerPage.Submit();

		LOG.info("==> Edit Customer successfully redirect to Edit Customer Page");
		assertEquals(editcustomerPage.getPageReturn(), editcustomerURL);
		LOG.info("URL: " + editcustomerPage.getPageReturn());

	}

	@Test
	public void TC_08_Address_cannot_be_empty() {

		LOG.info("1) Do not enter a value in ADDRESS Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.notEnterValueToName();

		LOG.info("==> An error message \"ADDRESS cannot be empty\" must be shown");
		assertEquals(editcustomerPage.getMessageOfAddressField(), Msg_CustomerField.ADDR_EMPTY);

	}

	@Test
	public void TC_09_City_cannot_be_empty() {

		LOG.info("1) Do not enter a value in CITY Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.notEnterValueToCity();

		LOG.info("==> An error message \"CITY cannot be empty\" must be shown");
		assertEquals(editcustomerPage.getMessageOfCityField(), Msg_CustomerField.CITY_EMPTY);

	}

	@Test
	public void TC_10_City_cannot_be_numeric() {

		LOG.info("1) Enter numeric value in CITY Field");
		editcustomerPage.enterValueToCity("223");

		LOG.info("==> An error message \"City cannot contain Number\" must be shown");
		assertEquals(editcustomerPage.getMessageOfCityField(), Msg_CustomerField.NUMBERIC);

	}

	@Test
	public void TC_11_City_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In CITY Field");
		editcustomerPage.enterValueToCity("c@@");

		LOG.info("==> An error message \"City cannot contain Special Characters\" must be shown");
		assertEquals(editcustomerPage.getMessageOfCityField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_12_state_cannot_be_empty() {

		LOG.info("1) Do not enter a value in STATE Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.setStateEmpty();

		LOG.info("==> An error message \"STATE cannot be empty\" must be shown");
		assertEquals(editcustomerPage.getMessageOfStateField(), Msg_CustomerField.STATE_EMPTY);

	}

	@Test
	public void TC_13_State_cannot_be_numeric() {

		LOG.info("1) Enter numeric value in STATE Field");
		editcustomerPage.enterValueToState("1234");

		LOG.info("==> An error message \"Numbers are not allowed\" must be shown");
		assertEquals(editcustomerPage.getMessageOfStateField(), Msg_CustomerField.NUMBERIC);

	}

	@Test
	public void TC_14_State_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In STATE Field");
		editcustomerPage.enterValueToState("s#$");

		LOG.info("==> An error message \"City cannot contain Special Characters\" must be shown");
		assertEquals(editcustomerPage.getMessageOfStateField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_15_PIN_must_be_numeric() {

		LOG.info("1) Enter character value in PIN Field");
		editcustomerPage.enterValueToPIN("pin");

		LOG.info("An error message \"PIN cannot contain character\" must be shown");
		assertEquals(editcustomerPage.getMessageOfPINField(), Msg_CustomerField.CHARACTER);

	}

	@Test
	public void TC_16_PIN_cannot_be_empty() {

		LOG.info("1) Do not enter a value in PIN Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.setPINEmpty();

		LOG.info("==> An error message \"PIN cannot be empty\" must be shown");
		assertEquals(editcustomerPage.getMessageOfPINField(), Msg_CustomerField.PIN_EMPTY);

	}

	@Test
	public void TC_17_PIN_must_have_6_digits() {

		LOG.info("1) Enter More than 6 digit in PIN field");
		LOG.info("2) Enter Less Than 6 digit in PIN field");
		editcustomerPage.enterValueToPIN(Customer.Pin_short);

		LOG.info("==> An error message \"PIN must contain 6 digit\"");
		assertEquals(editcustomerPage.getMessageOfPINField(), Msg_CustomerField.PIN_SHORT);

	}

	@Test
	public void TC_18_PIN_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In PIN Field");
		editcustomerPage.enterValueToPIN(Customer.special);

		LOG.info("==> An error message \"PIN cannot contain Special Characters\" must be shown");
		assertEquals(editcustomerPage.getMessageOfPINField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_19_Telephone_cannot_be_empty() {

		LOG.info("1) Do not enter a value in Telephone Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.setPhoneEmpty();

		LOG.info("An error message \"Telephone cannot be empty\" must be shown");
		assertEquals(editcustomerPage.getMessageOfPhoneField(), Msg_CustomerField.PHONE_EMPTY);

	}

	@Test
	public void TC_20_Telephone_cannot_have_special_character() {

		LOG.info("1) Enter Special Character In PIN Field");
		editcustomerPage.enterValueToPhone(Customer.special);

		LOG.info("==> An error message \"Telephone cannot contain Special Characters\" must be shown");
		assertEquals(editcustomerPage.getMessageOfPhoneField(), Msg_CustomerField.SPECIAL);

	}

	@Test
	public void TC_21_Email_cannot_be_empty() {

		LOG.info("1) Do not enter a value in Email Field");
		LOG.info("2) Press TAB and move to next Field");
		editcustomerPage.setEmailEmpty();

		LOG.info("==> An error message \"Email cannot be empty\" must be shown");
		assertEquals(editcustomerPage.getMessageOfEmailField(), Msg_CustomerField.EMAIL_EMPTY);

	}

	@Test
	public void TC_22_Email_must_be_in_format() {

		LOG.info("Enter invalid email in Email field");
		editcustomerPage.enterValueToEmail(Customer.invalid_email);

		LOG.info("==> An error message \"Email must be in format example@example.com\" must be shown");
		assertEquals(editcustomerPage.getMessageOfEmailField(), Msg_CustomerField.EMAIL_INVALID);

	}

	@Test
	public void TC_23__Email_must_be_in_format() {

		LOG.info("Enter invalid email in Email field");
		editcustomerPage.enterValueToEmail(Customer.invalid_email);

		LOG.info("==> An error message \"Email must be in format example@example.com\" must be shown");
		assertEquals(editcustomerPage.getMessageOfEmailField(), Msg_CustomerField.EMAIL_INVALID);

	}

	@AfterClass
	public void afterClass() throws Exception {
		Xl.generateReport(Constants.EXCEL_PATH, Constants.EXCEL_NAME);
		driver.quit();
	}

}
