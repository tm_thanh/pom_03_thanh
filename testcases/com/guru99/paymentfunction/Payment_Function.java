package com.guru99.paymentfunction;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.xml.DOMConfigurator;
import org.automationtesting.excelreport.Xl;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.Constants;
import commons.LogEvent;
import msg.Msg_Account;
import msg.Msg_CustomerField;
import pages.BalanceEnquiryPage;
import pages.DeleteAccountPage;
import pages.DeleteCustomerPage;
import pages.DepositPage;
import pages.EditCustomerPage;
import pages.FundTransferPage;
import pages.HomePage;
import pages.LoginPage;
import pages.NewAccountPage;
import pages.NewCustomerPage;
import pages.PageFactory;
import pages.WithDrawalPage;
import testdata.Customer;

public class Payment_Function extends AbstractTest {
	WebDriver driver;
	HomePage homePage;
	LoginPage loginPage;
	EditCustomerPage editcustomerPage;
	NewCustomerPage newcustomerPage;
	NewAccountPage newaccountPage;
	DepositPage depositPage;
	WithDrawalPage withdrawalPage;
	FundTransferPage fundtransferPage;
	BalanceEnquiryPage balanceenquiryPage;
	DeleteAccountPage deleteaccountPage;
	DeleteCustomerPage deletecustomerPage;
	LogEvent LOG;

	// testdata
	static String customerID = null;
	static String accountID = null;
	String description = "deposit";
	String Type_Current = "Current";
	String amount_value = "5000";
	String Initial_amount = "50000";
	String current_balance = "55000";
	String withdrawl_desc = "withdraw";
	String withdrawal_value = "15000";
	String withdrawal_balance = "40000";
	String payeerno = "28103";
	String amount_transfer = "10000";
	String transfer_desc = "transfer";
	String amount_balance = "30000";

	// expected msg
	String msg_delete_account = "Account Deleted Sucessfully";
	String msg_delete_customer = "Customer deleted Successfully";
	String msg_transfer = "Fund Transfer Details";

	@Parameters({ "browser", "url" })
	@BeforeClass
	public void beforeClass(String browser, String url) {
		driver = openBrowser(browser, url);
		loginPage = PageFactory.openLoginPage(driver);
		DOMConfigurator.configure("resources\\log4j.xml");
		homePage = loginPage.LoginAs(Constants.USERNAME, Constants.PASSWORD);
		LOG = new LogEvent();
	}

	@Test
	public void TC_01_Create_new_customer_account_successfully() {

		LOG.info("Open New Customer Page");
		newcustomerPage = homePage.openNewCustomerPage(driver);
		LOG.info("Fill info into fields");
		newcustomerPage.enterValueToName(Customer.CustomerName);
		newcustomerPage.enterValueToDOB(Customer.DateOfBirth);
		newcustomerPage.enterValueToAddress(Customer.Address);
		newcustomerPage.enterValueToCity(Customer.City);
		newcustomerPage.enterValueToState(Customer.State);
		newcustomerPage.enterValueToPIN(Customer.Pin);
		newcustomerPage.enterValueToPhone(Customer.Mobile);
		newcustomerPage.enterValueToEmail(Customer.Email);
		newcustomerPage.enterValueToPass(Customer.Password);

		LOG.info("Click Submit button");
		newcustomerPage.clickSubmit();

		customerID = newcustomerPage.getCustomerID();
		LOG.info("==> Verify message displays with content 'Customer Registered Successfully!!!'");
		assertEquals(newcustomerPage.getMessageOfRegister(), Msg_CustomerField.MSG_CUSTOMER_REGISTERED);
		LOG.info("message: " + newcustomerPage.getMessageOfRegister());

	}

	@Test
	public void TC_02_Edit_customer_account_successfully() {

		LOG.info("Open Edit Customer page");
		editcustomerPage = newcustomerPage.openEditCustomerPage(driver);

		LOG.info("Enter CustomerID field");
		editcustomerPage.enterValueToCustomerID(customerID);
		editcustomerPage.Submit();

		LOG.info("Fill in Address, City, State, PIN, Mobile Number, E-mail fields");
		newcustomerPage.enterValueToAddress(Customer.up_Address);
		newcustomerPage.enterValueToCity(Customer.up_City);
		newcustomerPage.enterValueToState(Customer.up_State);
		newcustomerPage.enterValueToPIN(Customer.up_Pin);
		newcustomerPage.enterValueToPhone(Customer.up_Mobile);
		newcustomerPage.enterValueToEmail(Customer.up_Email);

		LOG.info("Click Submit button");
		editcustomerPage.submitUpdate();

		LOG.info("==> Verify message displays with content 'Customer details updated Successfully!!!'");
		assertEquals(newcustomerPage.getMessageOfRegister(), Msg_CustomerField.MSG_UPDATE_CUSTOMER);
		LOG.info("message: " + newcustomerPage.getMessageOfRegister());

	}

	@Test
	public void TC_03_Add_new_account() {

		LOG.info("Open New Account page");
		newaccountPage = homePage.openNewAccountPage(driver);

		LOG.info("Fill in CustomerID, Account Type, Initial deposit fields");
		newaccountPage.enterValueToCustomerID(Customer.ID);
		newaccountPage.selectAccountType(Type_Current);
		newaccountPage.enterValueToDeposit(Initial_amount);

		LOG.info("Click Submit button");
		newaccountPage.submitAdd();

		LOG.info("==> - Verify message displays with content 'Account Generated Successfully!!!'");
		assertEquals(newaccountPage.getMessageofAccount(), Msg_Account.SUCCESS);
		accountID = newaccountPage.getAccountID();
		LOG.info("message: " + newaccountPage.getMessageofAccount());

		LOG.info("- Current Amount = 50,000");
		assertEquals(newaccountPage.getAmout(), "50000");

	}

	@Test
	public void TC_04_Transfer_money_into_a_current_account() {

		LOG.info("Open Deposit page");
		depositPage = homePage.openDepositPage(driver);

		LOG.info("Fill in Account No, Amount, Description fields");
		depositPage.enterValueToAccountID(accountID);
		depositPage.enterValueToAmount(amount_value);
		depositPage.enterValueToDescription(description);

		LOG.info("Click Submit button");
		depositPage.submit();

		LOG.info("==> - Verify message displays with content 'Transaction details of Deposit for Account Number "
				+ accountID);
		String msg_deposit = "Transaction details of Deposit for Account " + accountID;
		assertEquals(depositPage.getMessageOfDeposit(), msg_deposit);
		LOG.info("==> Current Balance: " + depositPage.getCurrentBalance());
		assertEquals(depositPage.getCurrentBalance(), current_balance);

	}

	@Test
	public void TC_05_Withdraw_money_from_current_account() {

		LOG.info("Open Withdrawal page");
		withdrawalPage = homePage.openWithDrawalPage(driver);

		LOG.info("Fill in Account No, Amount, Description fields");
		withdrawalPage.enterValueToAccountID(accountID);
		withdrawalPage.enterValueToAmount(withdrawal_value);
		withdrawalPage.enterValueToDescription(withdrawl_desc);

		LOG.info("Click Submit button");
		withdrawalPage.submit();

		LOG.info(
				"==> Verify message displays with content 'Transaction details of Withdrawal for Account " + accountID);
		String msg_withdrawal = "Transaction details of Withdrawal for Account " + accountID;
		assertEquals(withdrawalPage.getMessageOfWithdrawal(), msg_withdrawal);
		LOG.info("==> Current Balance: " + withdrawalPage.getCurrentBalance());
		assertEquals(withdrawalPage.getCurrentBalance(), withdrawal_balance);

	}

	@Test
	public void TC_06_Transfer_the_money_into_another_account() {

		LOG.info("Open Fund Transfer page");
		fundtransferPage = homePage.openFundTransferPage(driver);

		LOG.info("Fill in Payers account no, Payeers accountno, Amount, Description fields");
		fundtransferPage.enterValueToPayerAccount(accountID);
		fundtransferPage.enterValueToPayeerAccount(payeerno);
		fundtransferPage.enterValueToAmout(amount_transfer);
		fundtransferPage.enterValueToDescription(transfer_desc);

		LOG.info("Click Submit button");
		fundtransferPage.submitTransfer();

		LOG.info("==> Verify message displays with content Balance Details for Account " + payeerno);
		assertEquals(fundtransferPage.getMessageTransfer(), msg_transfer);
		assertEquals(fundtransferPage.getAmoutTransfer(), amount_transfer);

	}

	@Test
	public void TC_07_Check_current_account_balance_equal_30000() {

		LOG.info("Open Balance Enquiry page");
		balanceenquiryPage = homePage.openBalanceEnquiryPage(driver);

		LOG.info("Fill in Account No field");
		balanceenquiryPage.enterValueToAccountNo(accountID);

		LOG.info("Click Submit button");
		balanceenquiryPage.submit();

		LOG.info("==> Verify message displays with content Balance Details for Account " + accountID);

		String msg_balance = "Balance Details for Account " + accountID;
		assertEquals(balanceenquiryPage.getMessageBalance(), msg_balance);
		assertEquals(balanceenquiryPage.getAmoutBalance(), amount_balance);

	}

	@Test
	public void TC_08_Delete_all_accounts_of_this_customer_account() {

		LOG.info("Open Delete Account page");
		deleteaccountPage = homePage.openDeleteAccountPage(driver);

		LOG.info("Input AccountID");
		deleteaccountPage.enterValueToAccountNo(accountID);

		LOG.info("Click Submit button");
		deleteaccountPage.submit();

		LOG.info("==> Verify message displays with content 'Account Deleted Successfully!!!'");
		assertEquals(deleteaccountPage.getMessageOfDelete(), msg_delete_account);

	}

	@Test
	public void TC_09_Delete_exist_customer_account() {

		LOG.info("Open Delete Customer page");
		deletecustomerPage = homePage.openDeleteCustomerPage(driver);

		LOG.info("Input CustomerID");
		deletecustomerPage.enterValueToCustomerID(customerID);

		LOG.info("Click Submit button");
		deletecustomerPage.submit();

		LOG.info("==> Verify message displays with content 'Customer Deleted Successfully!!!'");
		assertEquals(deletecustomerPage.getMessageOfDelete(), msg_delete_customer);

	}

	@AfterClass
	public void afterClass() throws Exception {
		Xl.generateReport(Constants.EXCEL_PATH, Constants.EXCEL_NAME);
		driver.quit();
	}

}
