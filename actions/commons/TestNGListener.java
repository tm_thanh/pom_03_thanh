package commons;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListener implements ITestListener {
	LogEvent LOG = new LogEvent();

	@Override
	public void onFinish(ITestContext arg0) {

	}

	@Override
	public void onStart(ITestContext arg0) {

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	@Override
	public void onTestFailure(ITestResult result) {
		LOG.info("====> FAILED");
		LOG.endTestCase();

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		LOG.info("====> SKIPPED");
		LOG.endTestCase();

	}

	@Override
	public void onTestStart(ITestResult result) {
		LOG.startTestCase(result.getName());

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		LOG.info("====> PASSED");
		LOG.endTestCase();
	}

}
