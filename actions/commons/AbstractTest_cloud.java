package commons;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class AbstractTest_cloud {
	// using browserstack
	WebDriver driver;
	public static final String USERNAME = "thanhtruong5";
	public static final String AUTOMATE_KEY = "1SBzNRMA7mZssfKwiSVQ";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	public WebDriver openBrowser(String browser, String url) throws Exception {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browser", browser);
		caps.setCapability("browser_version", "50.0");
		caps.setCapability("os", "Windows");
		caps.setCapability("os_version", "7");
		caps.setCapability("browserstack.debug", "true");
		caps.setCapability("project", "BankGuru99");

		WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
		driver.get(url);
		return driver;
	}
}
