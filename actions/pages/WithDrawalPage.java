package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.WithDrawalPageUI;
import commons.AbstractPage;

public class WithDrawalPage extends AbstractPage {
	WebDriver driver;

	public WithDrawalPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterValueToAccountID(String value) {
		typeToElement(driver, DepositPage.ACCOUNT_ID, value);
	}

	public void enterValueToAmount(String value) {
		typeToElement(driver, DepositPage.AMOUNT_FIELD, value);
	}

	public void enterValueToDescription(String value) {
		typeToElement(driver, DepositPage.DESCRIPTION_FIELD, value);
	}

	public void submit() {
		clickToElement(driver, WithDrawalPageUI.SUBMIT_BTN);
	}

	public String getMessageOfWithdrawal() {
		return getTextOfElement(driver, WithDrawalPageUI.MESSAGE);
	}

	public String getCurrentBalance() {
		return getTextOfElement(driver, WithDrawalPageUI.BALANCE);
	}

}
