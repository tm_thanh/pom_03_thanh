package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.BalanceEnquiryPageUI;
import bankguru99.EditCustomerPageUI;
import commons.AbstractPage;

public class DeleteCustomerPage extends AbstractPage {
	WebDriver driver;

	public DeleteCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterValueToCustomerID(String value) {
		typeToElement(driver, EditCustomerPageUI.CUSTOMERID_FIELD, value);
	}

	public String getMessageOfDelete() {
		String content = getTextAlert(driver);
		acceptAlert(driver);
		return content;
	}

	public void submit() {
		clickToElement(driver, BalanceEnquiryPageUI.SUBMIT_BTN);
		acceptAlert(driver);

	}

}
