package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.BalanceEnquiryPageUI;
import commons.AbstractPage;

public class DeleteAccountPage extends AbstractPage {
	WebDriver driver;

	public DeleteAccountPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterValueToAccountNo(String value) {
		typeToElement(driver, DepositPage.ACCOUNT_ID, value);

	}

	public void submit() {
		clickToElement(driver, BalanceEnquiryPageUI.SUBMIT_BTN);
		acceptAlert(driver);

	}

	public String getMessageOfDelete() {
		String content = getTextAlert(driver);
		acceptAlert(driver);
		return content;
	}

}
