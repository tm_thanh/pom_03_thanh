package pages;

import org.openqa.selenium.WebDriver;

public class PageFactory {
	public static LoginPage openLoginPage(WebDriver driver) {
		return new LoginPage(driver);
	}

	public static HomePage getHomePage(WebDriver driver) {
		return new HomePage(driver);
	}

	public static NewCustomerPage getNewCustomerPage(WebDriver driver) {
		return new NewCustomerPage(driver);
	}

	public static EditCustomerPage getEditCustomerPage(WebDriver driver) {
		return new EditCustomerPage(driver);
	}

	public static NewAccountPage getNewAccountPage(WebDriver driver) {
		return new NewAccountPage(driver);
	}

	public static DepositPage getDepositPage(WebDriver driver) {
		return new DepositPage(driver);
	}

	public static WithDrawalPage getWithDrawalPage(WebDriver driver) {
		return new WithDrawalPage(driver);
	}

	public static FundTransferPage getFundTransferPage(WebDriver driver) {
		return new FundTransferPage(driver);
	}

	public static BalanceEnquiryPage getBalanceEnquiryPage(WebDriver driver) {
		return new BalanceEnquiryPage(driver);
	}

	public static DeleteAccountPage getDeleteAccountPage(WebDriver driver) {
		return new DeleteAccountPage(driver);
	}

	public static DeleteCustomerPage getDeleteCustomerPage(WebDriver driver) {
		return new DeleteCustomerPage(driver);
	}

}
