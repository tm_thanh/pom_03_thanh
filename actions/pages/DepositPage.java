package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.DepositPageUI;
import commons.AbstractPage;

public class DepositPage extends AbstractPage {
	WebDriver driver;

	public DepositPage(WebDriver driver) {
		this.driver = driver;
	}

	static String ACCOUNT_ID = String.format(DepositPageUI.D_FIELDS, "accountno");
	static String AMOUNT_FIELD = String.format(DepositPageUI.D_FIELDS, "ammount");
	static String DESCRIPTION_FIELD = String.format(DepositPageUI.D_FIELDS, "desc");

	public void enterValueToAccountID(String value) {
		typeToElement(driver, ACCOUNT_ID, value);
	}

	public void enterValueToAmount(String value) {
		typeToElement(driver, AMOUNT_FIELD, value);
	}

	public void enterValueToDescription(String value) {
		typeToElement(driver, DESCRIPTION_FIELD, value);

	}

	public String getMessageOfDeposit() {
		return getTextOfElement(driver, DepositPageUI.MESSAGE);
	}

	public String getCurrentBalance() {
		return getTextOfElement(driver, DepositPageUI.BALANCE);
	}

	public void submit() {
		clickToElement(driver, DepositPageUI.SUBMIT_BTN);
		
	}


}
