package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.NewAccountPageUI;
import commons.AbstractPage;

public class NewAccountPage extends AbstractPage {
	WebDriver driver;

	public NewAccountPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterValueToCustomerID(String value) {
		typeToElement(driver, NewAccountPageUI.CustomerID, value);
	}

	public void selectAccountType(String value) {
		selectDropdownlist(driver, NewAccountPageUI.ACCOUNT_TYPE, value);
	}

	public void enterValueToDeposit(String value) {
		typeToElement(driver, NewAccountPageUI.DEPOSIT, value);
	}

	public void submitAdd() {
		clickToElement(driver, NewAccountPageUI.SUBMIT_BTN);
	}

	public String getMessageofAccount() {
		return getTextOfElement(driver, NewAccountPageUI.MESSAGE);
	}

	public String getAmout() {
		return getTextOfElement(driver, NewAccountPageUI.RETURN_AMOUNT);
	}

	public String getAccountID() {
		return getTextOfElement(driver, NewAccountPageUI.RETURN_ACCOUNTID);
	}

}
