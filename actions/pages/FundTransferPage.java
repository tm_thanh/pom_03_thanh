package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.FundTransferPageUI;
import commons.AbstractPage;

public class FundTransferPage extends AbstractPage {
	WebDriver driver;

	public FundTransferPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterValueToPayerAccount(String value) {
		String PAYER_NO = String.format(FundTransferPageUI.D_FIELD, "payersaccount");
		typeToElement(driver, PAYER_NO, value);
	}

	public void enterValueToPayeerAccount(String value) {
		String PAYEER_NO = String.format(FundTransferPageUI.D_FIELD, "payeeaccount");
		typeToElement(driver, PAYEER_NO, value);

	}

	public void enterValueToAmout(String value) {
		String TRANSFER_AMOUNT = String.format(FundTransferPageUI.D_FIELD, "ammount");
		typeToElement(driver, TRANSFER_AMOUNT, value);

	}

	public void enterValueToDescription(String value) {
		String TRANSFER_DESC = String.format(FundTransferPageUI.D_FIELD, "desc");
		typeToElement(driver, TRANSFER_DESC, value);

	}

	public void submitTransfer() {
		String SUBMIT_BTN = String.format(FundTransferPageUI.D_FIELD, "AccSubmit");
		clickToElement(driver, SUBMIT_BTN);

	}

	public String getAmoutTransfer() {
		return getTextOfElement(driver, FundTransferPageUI.AMOUNT);
	}

	public String getMessageTransfer() {
		return getTextOfElement(driver, FundTransferPageUI.MESSAGE);
	}

}
