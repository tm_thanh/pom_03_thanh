package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.BalanceEnquiryPageUI;
import commons.AbstractPage;

public class BalanceEnquiryPage extends AbstractPage {
	WebDriver driver;

	public BalanceEnquiryPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterValueToAccountNo(String value) {
		typeToElement(driver, BalanceEnquiryPageUI.ACCOUNT_NO, value);
	}

	public void submit() {
		clickToElement(driver, BalanceEnquiryPageUI.SUBMIT_BTN);
		
	}

	public String getMessageBalance() {
		return getTextOfElement(driver, BalanceEnquiryPageUI.MESSAGE);
	}

	public String getAmoutBalance() {
		return getTextOfElement(driver, BalanceEnquiryPageUI.AMOUNT);
	}
}
