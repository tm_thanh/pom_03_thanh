package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.EditCustomerPageUI;
import bankguru99.NewCustomerPageUI;
import commons.AbstractPage;

public class EditCustomerPage extends AbstractPage {

	WebDriver driver;
	String MSG_CUSTOMERID = String.format(EditCustomerPageUI.D_MSG_EDITCUSTOMER, "message14");

	public EditCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	public void notEnterValueToCustomerID() {
		clickToElement(driver, EditCustomerPageUI.CUSTOMERID_FIELD);
		pressTABkey(driver);
	}

	public String getMessageOfCustomerIDField() {
		return getTextOfElement(driver, MSG_CUSTOMERID);
	}

	public void enterValueToCustomerID(String value) {
		typeToElement(driver, EditCustomerPageUI.CUSTOMERID_FIELD, value);

	}

	public void Submit() {
		clickToElement(driver, EditCustomerPageUI.SUBMIT_BTN);

	}

	public String getPageReturn() {
		return getURL(driver);
	}

	public void notEnterValueToName() {
		clearTextOfElement(driver, NewCustomerPageUI.ADDRESS_FIELD);
		pressTABkey(driver);

	}

	public String getMessageOfAddressField() {
		return getTextOfElement(driver, NewCustomerPage.MSG_ADDR_FIELD);
	}

	public void notEnterValueToCity() {
		clearTextOfElement(driver, NewCustomerPage.CITY_FIELD);
		pressTABkey(driver);

	}

	public String getMessageOfCityField() {
		return getTextOfElement(driver, NewCustomerPage.MSG_CITY_FIELD);
	}

	public void enterValueToCity(String value) {
		typeToElement(driver, NewCustomerPage.CITY_FIELD, value);
	}

	public void setStateEmpty() {
		clearTextOfElement(driver, NewCustomerPage.STATE_FIELD);
		pressTABkey(driver);
	}

	public String getMessageOfStateField() {
		return getTextOfElement(driver, NewCustomerPage.MSG_STATE_FIELD);
	}

	public void enterValueToState(String value) {
		typeToElement(driver, NewCustomerPage.STATE_FIELD, value);

	}

	public void enterValueToPIN(String value) {
		typeToElement(driver, NewCustomerPage.PIN_FIELD, value);

	}

	public String getMessageOfPINField() {
		return getTextOfElement(driver, NewCustomerPage.MSG_PIN_FIELD);
	}

	public void setPINEmpty() {
		clearTextOfElement(driver, NewCustomerPage.PIN_FIELD);
		pressTABkey(driver);

	}

	public void setPhoneEmpty() {
		clearTextOfElement(driver, NewCustomerPage.PHONE_FIELD);
		pressTABkey(driver);

	}

	public String getMessageOfPhoneField() {
		return getTextOfElement(driver, NewCustomerPage.MSG_PHONE_FIELD);
	}

	public void enterValueToPhone(String value) {
		typeToElement(driver, NewCustomerPage.PHONE_FIELD, value);

	}

	public void setEmailEmpty() {
		clearTextOfElement(driver, NewCustomerPage.EMAIL_FIELD);
		pressTABkey(driver);

	}

	public String getMessageOfEmailField() {
		return getTextOfElement(driver, NewCustomerPage.MSG_EMAIL_FIELD);
	}

	public void enterValueToEmail(String value) {
		typeToElement(driver, NewCustomerPage.EMAIL_FIELD, value);
	}

	public void submitUpdate() {
		clickToElement(driver, EditCustomerPageUI.SUBMIT_UPDATE_BTN);		
	}


}
