package pages;

import org.openqa.selenium.WebDriver;

import bankguru99.LoginPageUI;
import commons.AbstractPage;

public class LoginPage extends AbstractPage {
	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	public void typeToUserName(String userName) {
		typeToElement(driver, LoginPageUI.USERNAME_TEXTBOX, userName);
	}

	public void typeToPassword(String password) {
		typeToElement(driver, LoginPageUI.PASSWORD_TEXTBOX, password);
	}

	public HomePage LoginAs(String userName, String password) {
		typeToUserName(userName);
		typeToPassword(password);
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);
		return PageFactory.getHomePage(driver);
		// return new HomePage(driver);
	}

}
