package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import bankguru99.NewCustomerPageUI;
import commons.AbstractPage;

public class NewCustomerPage extends AbstractPage {

	WebDriver driver;

	public NewCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	String NAME_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "name");
	String DOB_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "dob");
	static String CITY_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "city");
	static String STATE_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "state");
	static String PIN_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "pinno");
	static String PHONE_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "telephoneno");
	static String EMAIL_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "emailid");
	static String PASSWORD_FIELD = String.format(NewCustomerPageUI.D_INPUT_NEWCUSTOMER, "password");

	// xpath for msg
	String MSG_NAME_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message");
	static String MSG_ADDR_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message3");
	static String MSG_CITY_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message4");
	static String MSG_STATE_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message5");
	static String MSG_PIN_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message6");
	static String MSG_PHONE_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message7");
	static String MSG_EMAIL_FIELD = String.format(NewCustomerPageUI.D_MSG_NEWCUSTOMER, "message9");
	
	//register
	static String MSG_CUSTOMER_REGIS = "//p";

	public void notEnterValueIntoNAMEField() {
		clickToElement(driver, NAME_FIELD);
		clickToElement(driver, DOB_FIELD);
	}

	public String getMessageOfNameField() {
		return getTextOfElement(driver, MSG_NAME_FIELD);
	}

	public void enterValueToName(String value) {
		typeToElement(driver, NAME_FIELD, value);
	}

	public void notEnterValueToAddress() {
		clickToElement(driver, NewCustomerPageUI.ADDRESS_FIELD);
		clickToElement(driver, DOB_FIELD);

	}

	public String getMessageOfAddressField() {
		return getTextOfElement(driver, MSG_ADDR_FIELD);
	}

	public void enterValueToAddress(String value) {
		typeToElement(driver, NewCustomerPageUI.ADDRESS_FIELD, value);

	}

	public void notEnterValueToCity() {
		clickToElement(driver, CITY_FIELD);
		clickToElement(driver, DOB_FIELD);
	}

	public String getMessageOfCityField() {
		return getTextOfElement(driver, MSG_CITY_FIELD);
	}

	public void enterValueToCity(String value) {
		typeToElement(driver, CITY_FIELD, value);
	}

	public void notEnterValueToState() {
		clickToElement(driver, STATE_FIELD);
		clickToElement(driver, CITY_FIELD);

	}

	public String getMessageOfStateField() {
		return getTextOfElement(driver, MSG_STATE_FIELD);
	}

	public void enterValueToState(String value) {
		typeToElement(driver, STATE_FIELD, value);
	}

	public void enterValueToPIN(String value) {
		typeToElement(driver, PIN_FIELD, value);
	}

	public String getMessageOfPINField() {
		return getTextOfElement(driver, MSG_PIN_FIELD);
	}

	public void notEnterValueToPIN() {
		clickToElement(driver, PIN_FIELD);
		clickToElement(driver, CITY_FIELD);

	}

	public void notEnterValueToPhone() {
		clickToElement(driver, PHONE_FIELD);
		clickToElement(driver, CITY_FIELD);

	}

	public void enterValueToPhone(String value) {
		typeToElement(driver, PHONE_FIELD, value);
	}

	public void notEnterValueToEmail() {
		clickToElement(driver, EMAIL_FIELD);
		clickToElement(driver, CITY_FIELD);

	}

	public String getMessageOfPhoneField() {
		return getTextOfElement(driver, MSG_PHONE_FIELD);
	}

	public String getMessageOfEmailField() {
		return getTextOfElement(driver, MSG_EMAIL_FIELD);
	}

	public void enterValueToEmail(String value) {
		typeToElement(driver, EMAIL_FIELD, value);
	}

	public void enterValueToDOB(String value) {
		// typeToElement(driver, DOB_FIELD, value);
		WebElement element = driver.findElement(By.xpath(DOB_FIELD));
		element.sendKeys(value);
	}

	public void enterValueToPass(String value) {
		typeToElement(driver, PASSWORD_FIELD, value);
	}

	public void clickSubmit() {
		clickToElement(driver, NewCustomerPageUI.SUBMIT_BTN);

	}

	public String getMessageOfRegister() {
		return getTextOfElement(driver, MSG_CUSTOMER_REGIS);
	}

	public String getCustomerID() {
		return getTextOfElement(driver, NewCustomerPageUI.GET_CUSTOMER_ID);
	}

}
