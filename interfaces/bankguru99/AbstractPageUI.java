package bankguru99;

public class AbstractPageUI {
	public static final String EDIT_CUSTOMER_PAGE = "//a[contains(text(),'Edit Customer')]";
	public static final String HOME_PAGE = "//a[contains(text(),'Manager')]";
	public static final String NEW_CUSTOMER_PAGE = "//a[contains(text(),'New Customer')]";
	public static final String NEW_ACCCOUNT_PAGE = "//a[contains(text(),'New Account')]";
	public static final String DEPOSIT_PAGE = "//a[contains(text(),'Deposit')]";
	public static final String WITHDRAWAL_PAGE = "//a[contains(text(),'Withdrawal')]";
	public static final String FUNDTRANSFER_PAGE = "//a[contains(text(),'Fund Transfer')]";
	public static final String BALANCEENQUIRY_PAGE ="//a[contains(text(),'Balance Enquiry')]";
	public static final String DELETEACCOUNT_PAGE ="//a[contains(text(),'Delete Account')]";
	public static final String DELETECUSTOMER_PAGE="//a[contains(text(),'Delete Customer')]";

}
