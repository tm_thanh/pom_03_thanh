package bankguru99;

public class NewAccountPageUI {

	public static String CustomerID = "//input[@name='cusid']";
	public static String ACCOUNT_TYPE = "//select[@name='selaccount']";
	public static String DEPOSIT = "//input[@name='inideposit']";
	public static String SUBMIT_BTN = "//input[@name='button2']";
	public static String MESSAGE = "//table[@id='account']//tr[1]";
	public static String RETURN_AMOUNT = "//table[@id='account']//tr[10]/td[2]";
	public static String RETURN_ACCOUNTID = "//table[@id='account']//tr[4]/td[2]";

}
