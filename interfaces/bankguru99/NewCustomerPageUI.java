package bankguru99;

public class NewCustomerPageUI {
	// dynamic xpath
	public static final String D_INPUT_NEWCUSTOMER = "//input[@name='%s']";
	public static final String D_MSG_NEWCUSTOMER = "//label[@id='%s']";

	// static xpath
	public static final String ADDRESS_FIELD = "//textarea[@name='addr']";
	public static final String SUBMIT_BTN = "//input[@name='sub']";
	public static String GET_CUSTOMER_ID = "//table[@id='customer']//tr[4]/td[2]";

}
