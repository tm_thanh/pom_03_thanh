package bankguru99;

public class EditCustomerPageUI {
	public static final String CUSTOMERID_FIELD = "//input[@name='cusid']";
	public static final String SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String SUBMIT_UPDATE_BTN ="//input[@name='sub']";

	// MSG
	public static final String D_MSG_EDITCUSTOMER = "//label[@id='%s']";

}
