package testdata;

import org.apache.commons.lang3.RandomStringUtils;

public class Customer {

	public static String random(int number) {
		return RandomStringUtils.randomAlphabetic(number);
	}

	// invalid
	public static String Character = "abc";
	public static String special = "#$#";
	public static String Pin_short = "434";
	public static String invalid_email = "abc@";
	public static String numberic = "123";
	public static String number_space ="12 123";
	public static String email_space="testing auto@";

	// valid
	public static String CustomerName = "Thanh " + random(4);
	public static String DateOfBirth = "01/01/1989";
	public static String Address = "PO Box 911 8331 Duis Avenue";
	public static String City = "Tampa";
	public static String State = "FL";
	public static String Pin = "466250";
	public static String Mobile = "4555442476";
	public static String Email = random(3) + "thanh_automation@gmail.com";
	public static String Password = "automation";
	public static String ID = "62626";

	// update
	public static String up_Address = "= 1883 Cursus Avenue";
	public static String up_City = "Houson";
	public static String up_State = "Texas";
	public static String up_Pin = "166455";
	public static String up_Mobile = "4779728081";
	public static String up_Email = random(3) + "update_automation@gmail.com";



}
