package msg;

public class Msg_CustomerField {
	public static final String CUSTOMER_EMPTY = "Customer ID is required";
	public static final String CHARACTER = "Characters are not allowed";
	public static final String SPECIAL = "Special characters are not allowed";
	public static final String ADDR_EMPTY = "Address Field must not be blank";
	public static final String CITY_EMPTY = "City Field must not be blank";
	public static final String NUMBERIC = "Numbers are not allowed";
	public static final String STATE_EMPTY = "State must not be blank";
	public static final String PIN_EMPTY = "PIN Code must not be blank";
	public static final String PIN_SHORT = "PIN Code must have 6 Digits";
	public static final String PHONE_EMPTY = "Mobile no must not be blank";
	public static final String EMAIL_EMPTY = "Email-ID must not be blank";
	public static final String EMAIL_INVALID = "Email-ID is not valid";
	public static final String NAME_EMPTY = "Customer name must not be blank";
	public static final String SPACE_FIRST = "First character can not have space";
	public static final String EMAIL_SPACE = "Email-ID is not valid";

	//
	public static final String MSG_CUSTOMER_REGISTERED = "Customer Registered Successfully!!!";
	public static final String MSG_UPDATE_CUSTOMER = "Customer details updated Successfully!!!";

}
